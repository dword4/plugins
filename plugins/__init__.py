#!/usr/bin/env python3

import importlib
import importlib.util
import sys
from pathlib import Path
import logging
from os.path import exists

class Plugins(object):

    def __init__(self):

        # define some lists
        self.plugins = []
        self.all_methods = []
        self.callable_methods = {}
        self.found_plugins_obj = []

        pathlist = Path('plugins/').glob('**/plugin.py')
        plugin_paths = []
        for path in pathlist:
            plugin_paths.append(path)
            path_split = str(path).split('/')
            plugin_name = path_split[1]
            self.plugins.append(plugin_name)
            spec = importlib.util.spec_from_file_location(plugin_name, path)
            foo = importlib.util.module_from_spec(spec)
            spec.loader.exec_module(foo)

            # get available method names in all of our plugins
            found_methods = {foo.Plugin.__module__:[m for m in dir(foo.Plugin) if not m.startswith('__')]}
            
            # append found_methods to callable_methods dict
            self.callable_methods = {**self.callable_methods, **found_methods}

            # store our plugin object in a list so we can access it later
            self.found_plugins_obj.append(foo)
