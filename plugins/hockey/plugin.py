#!/usr/bin/env python3

import requests
import arrow
import json

class Plugin(object):
    """ Hockey system """
    def __init__(self):
        self.version = '1.0'
        self.name = 'hockey'
        self.parts = [
                {'obj':self, 'method':'schedule', 'command':'!schedule', 'admin': False, 'help': self.schedule.__doc__},
                {'obj':self, 'method':'standings', 'command':'!standings', 'admin': False},
                {'obj':self, 'method':'todays_games', 'command':'!nhl_games', 'admin': False}
            ]
        print('initialize hockey features')

    def getTeamAbbr(*args):
        ID = args[1]
        ret = False
        teams = {
            "NJD": 1,
            "NYI": 2,
            "NYR": 3,
            "PHI": 4,
            "PIT": 5,
            "BOS": 6,
            "BUF": 7,
            "MTL": 8,
            "OTT": 9,
            "TOR": 10,
            "CAR": 12,
            "FLA": 13,
            "TBL": 14,
            "WSH": 15,
            "CHI": 16,
            "DET": 17,
            "NSH": 18,
            "STL": 19,
            "CGY": 20,
            "COL": 21,
            "EDM": 22,
            "VAN": 23,
            "ANA": 24,
            "DAL": 25,
            "LAK": 26,
            "SJS": 28,
            "CBJ": 29,
            "MIN": 30,
            "WPG": 52,
            "ARI": 53,
            "VGK": 54,
        }
        for team, teamId in teams.items():
            if teamId == ID:
                ret = team
        if ret == False:
            sub_req = "https://statsapi.web.nhl.com/api/v1/teams/" + str(ID)
            sub_r = requests.get(sub_req)
            team_json = sub_r.text
            team_data = json.loads(team_json)
            ret = team_data["teams"][0]["abbreviation"]
        return ret 

    # get team ID from abbreviation
    def getTeamId(self, abbreviation):
        teams = {
            "NJD": 1,
            "NYI": 2,
            "NYR": 3,
            "PHI": 4,
            "PIT": 5,
            "BOS": 6,
            "BUF": 7,
            "MTL": 8,
            "OTT": 9,
            "TOR": 10,
            "CAR": 12,
            "FLA": 13,
            "TBL": 14,
            "WSH": 15,
            "CHI": 16,
            "DET": 17,
            "NSH": 18,
            "STL": 19,
            "CGY": 20,
            "COL": 21,
            "EDM": 22,
            "VAN": 23,
            "ANA": 24,
            "DAL": 25,
            "LAK": 26,
            "SJS": 28,
            "CBJ": 29,
            "MIN": 30,
            "WPG": 52,
            "ARI": 53,
            "VGK": 54,
        }
        team = abbreviation.upper()

        if team in teams:
            return teams[team.upper()]
        else:
            return false

    # convert 3 letter abbreviations to full names
    def abbrToFullname(self, abbr):
        teams = {
            "NJD": "New Jersey Devils",
            "NYI": "New York Islanders",
            "NYR": "New York Rangers",
            "PHI": "Philadelphia Flyers",
            "PIT": "Pittsburgh Penguins",
            "BOS": "Boston Bruins",
            "BUF": "Buffalo Sabres",
            "MTL": "Montreal Canadiens",
            "OTT": "Ottowa Senators",
            "TOR": "Toronto Maple Leafs",
            "CAR": "Carolina Hurricanes",
            "FLA": "Florida Panthers",
            "TBL": "Tampa Bay Lightning",
            "WSH": "Washington Capitals",
            "CHI": "Chicago Blackhawks",
            "DET": "Detroit Red Wings",
            "NSH": "Nashville Predators",
            "STL": "St. Louis Blues",
            "CGY": "Calgary Flames",
            "COL": "Colorado Avalanche",
            "EDM": "Edmonton Oilers",
            "VAN": "Vancouver Canucks",
            "ANA": "Anaheim Ducks",
            "DAL": "Dallas Stars",
            "LAK": "Los Angeles Kings",
            "SJS": "San Jose Sharkes",
            "CBJ": "Columbus Blue Jackets",
            "MIN": "Minnesota Wild",
            "WPG": "Winnipeg Jets",
            "ARI": "Arizona Coyotes",
            "VGK": "Vegas Golden Knights",
        }
        if abbr.upper() in teams.keys():
            return teams[abbr.upper()]
        else:
            return False

    # gets todays game details
    def todays_games(*args):
        self = args[0]
        print("checking games today")
        url = "https://statsapi.web.nhl.com/api/v1/schedule"
        r = requests.get(url).json()

        if r["totalGames"] == 0:
            print("no games today")
        else:
            print("games scheduled today")
            i = 0
            ret = ""
            limit = len(r["dates"][0]["games"]) - 1
            for game in r["dates"][0]["games"]:
                gameStatus = game["status"]["abstractGameState"]
                if gameStatus == "Live":
                    game_id = game["gamePk"]  # this is an int btw
                    game_score_request = (
                        "https://statsapi.web.nhl.com/api/v1/game/"
                        + str(game_id)
                        + "/linescore"
                    )
                    game_details = requests.get(game_score_request).json()
                    game_period = game_details["currentPeriodOrdinal"]

                    team_away_id = game["teams"]["away"]["team"]["id"]
                    team_home_id = game["teams"]["home"]["team"]["id"]
                    team_away_name = game["teams"]["away"]["team"]["name"]
                    team_home_name = game["teams"]["home"]["team"]["name"]
                    team_away_score = game_details["teams"]["away"]["goals"]
                    team_home_score = game_details["teams"]["home"]["goals"]
                    team_away_abbr = self.getTeamAbbr(team_away_id)
                    team_home_abbr = self.getTeamAbbr(team_home_id)
                    msg = "<b>%s</b>@<b>%s</b> %s-%s %s" % (
                        team_away_abbr,
                        team_home_abbr,
                        team_away_score,
                        team_home_score,
                        game_period,
                    )
                    # print("live:"+msg)
                elif gameStatus == "Final":
                    game_id = game["gamePk"]  # this is an int btw
                    game_score_request = (
                        "https://statsapi.web.nhl.com/api/v1/game/"
                        + str(game_id)
                        + "/linescore"
                    )
                    game_details = requests.get(game_score_request).json()

                    team_away_name = game["teams"]["away"]["team"]["name"]
                    team_home_name = game["teams"]["home"]["team"]["name"]
                    team_away_id = game["teams"]["away"]["team"]["id"]
                    team_home_id = game["teams"]["home"]["team"]["id"]
                    team_away_score = game_details["teams"]["away"]["goals"]
                    team_home_score = game_details["teams"]["home"]["goals"]
                    team_away_abbr = self.getTeamAbbr(team_away_id)
                    team_home_abbr = self.getTeamAbbr(team_home_id)
                    msg = r"<b>%s</b> @ <b>%s</b> %s-%s Final" % (
                        team_away_abbr,
                        team_home_abbr,
                        team_away_score,
                        team_home_score,
                    )
                    """
                                        using ** and __ to bold in the msg varaible does not work
                                        """
                    # print("final:"+msg)
                else:
                    game_state = game["status"]["detailedState"]
                    game_time = game["gameDate"]
                    team_away_name = game["teams"]["away"]["team"]["name"]
                    team_home_name = game["teams"]["home"]["team"]["name"]
                    team_away_id = game["teams"]["away"]["team"]["id"]
                    team_home_id = game["teams"]["home"]["team"]["id"]
                    team_away_score = game["teams"]["away"]["score"]
                    team_home_score = game["teams"]["home"]["score"]
                    team_away_abbr = self.getTeamAbbr(team_away_id)
                    team_home_abbr = self.getTeamAbbr(team_home_id)
                    a_game_time = arrow.get(game_time)
                    game_time_local = a_game_time.to("US/Eastern").format("HHmm")
                    msg = "<b>%s</b>@<b>%s</b> at %s EST" % (
                        team_away_abbr,
                        team_home_abbr,
                        game_time_local,
                    )
                    # print("other:"+msg)
                if i == 0:
                    ret += msg
                    ret += " / "
                elif i == limit:
                    ret += msg
                else:
                    ret += msg
                    ret += " / "
                i += 1
            print(ret)
        return ret

    def schedule(*args):
        """ either displays the entire schedule for today or gets the schedule for a specific teamId """
        teamID = args[1]
        msg = f"{teamID} schedule lookup"
        return msg

    def standings(*args):
        """ shows standings """
        msg = "standings can be displayed here"
        return msg
